# Chuck Callebs

### Summary
I'm a 26 year old software developer living in Richmond, KY. I've been programming in some capacity since I was 11 years old -- never considering another profession. I'm passionate about readable (testable) code, pragmatic approaches, remote working, and building useful things. When not at a computer, you can find me spending time with my wife, at the gym, or hiking in the woods.

### Personal Details
* Email: cacallebs@gmail.com
* Phone: (512) 579-6614
* Twitter: [@ccallebs](http://www.twitter.com/ccallebs)
* GitHub: [ccallebs](http://www.github.com/ccallebs)
* Website: [http://callebs.io](http://callebs.io)
* StackOverflow: [http://stackoverflow.com/users/14877](http://stackoverflow.com/users/14877)
* RubyGems: [http://rubygems.org/profiles/ccallebs](http://rubygems.org/profiles/ccallebs)

### Languages / Frameworks
* Ruby (Rails, Sinatra, Gosu)
* Node.JS (Express)
* JavaScript (Backbone, jQuery)
* CoffeeScript
* C/C++
* C# (ASP.NET WebForms/MVC)
* PHP

### Interests
* Value Dependent Typing
* Functional Programming

### Experience

#### __Senior Ruby on Rails Engineer__ at __Tugg, Inc.__

*08/2012 - Present*

__Technologies used:__ Ruby on Rails, JavaScript, CoffeeScript, Node.js (Express), MySQL, PostgreSQL

* Actively developed 5 largely de-coupled codebases, a result of advocacy to break our functionality into single-purpose, manageable applications.
* Significantly refactored a broken, short-sighted legacy application handed over from contractors. Page load time drastically decreased, test coverage increased.
* Built internal administration tools to handle an ever-increasing number of events.
* Handled most DevOps needs -- performing server upgrades, setting up staging environments, maintaining our deploy script.
* Mentored less experienced developers by performing code reviews and encouraging peer programming.

#### __Consultant__ at __Litmoose, LLC__

*04/2010 - Present*

__Technologies used:__ Ruby on Rails, CoffeeScript, AngularJS, ASP.NET

* Consulted a number of projects providing custom software solutions.
* Worked with: [DoStuff Media](http://www.dostuffmedia.com), [Bodireel](http://www.bodireel.com/), [Sonata Cove Software](http://sonatacove.com/) [PostAnEvent](http://postanevent.com/), [Adcolor, Inc.](http://adcolorinc.com)

#### __Applications Developer__ at __Heat Transfer Research, Inc.__

*07/2011 - 08/2012*

__Technologies used:__ C#, ASP.NET MVC 3, C++, Fortran

* Worked in the Enterprise Applications Group to make modifications to HTRI’s XchangerSuite software suite (specifically XchangerSuite 7)
* Developed an updated security module leveraging a mix of hardware (USB dongles) and software authentication to better protect HTRI intellectual property while also improving end-user flexibility. Used C++ to develop the core functionality, but implemented the module in both C# and Fortran solutions.
* Created forward-facing licensing application using this security module to handle distribution of hardware and software licenses.
* Developed web application in ASP.NET MVC 3 that interfaced with multiple databases to provide a more user-friendly portal to frequently used information.

#### __Software Developer__ at __Intellithought, Inc.__

*11/2010 - 06/2011*

__Technologies used:__ C#, VB.NET, ASP.NET MVC 2, Visual FoxPro, SQL Server 2008

* Worked closely with multiple clients confined by strict deadlines / budgets to develop custom software solutions.
* Maintained a legacy Visual FoxPro application using a proprietary framework. Due to maintenance overhead, embedded a browser in the application and built new functionality as a web application.
* Created a factory management tool for Domtar to help streamline processes. Used serialized JSON to dynamically create custom forms and store the values.

#### __Software Developer__ at __Master Meter Systems__

__Technologies used:__ C#, ASP.NET WebForms, Crystal Reports, SQL Server 2005

*05/2010 - 10/2010*

* Built new functionality for enterprise web application serving hundreds of thousands of customers.
* Built reports from tables containing over 200 million rows. Used Crystal Reports and SQL Server stored procedures.
* Started initiative to use LINQ-to-SQL for non-critical tasks.

#### __Junior Software Developer__ at __Apax Software, LLC__

*11/2007 - 05/2009*

__Technologies used:__ VB.NET, ASP.NET WebForms, PHP, SQL Server 2000

* Built functionality for a military equipment requisition system (SSAVIENET) and vehicle maintenance site (FOSOV).
* Used VB.NET and SQL Server 2000.
* Learned many military acronyms.


### Personal Endeavors

__KerbalCrafts:__ [http://www.kerbalcrafts.com](http://www.kerbalcrafts.com)

A Kerbal Space Program ship sharing site. Built using Rails 3 and Amazon S3. Deployed on a Digital Ocean droplet.

__Typedo:__ [http://typedo.herokuapp.com](http://typedo.herokuapp.com)

A command-line based task management platform. Similar to todo.txt without the Dropbox depedency and vastly superior VIM-like syntax.

__Spousal Collaboration App:__ (in progress)

A tool to better sync the efforts and celebrate the milestones of couples. Built using Rails 4 + Backbone.

__Minepal (dead):__ http://www.minepal.com

A Minecraft server sharing site. Built using Rails 2.
